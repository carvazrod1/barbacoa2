== Posibles localizaciones

// Describir lugar y poner alguna foto. ¡Ojo!

=== Rivera del Huéznar

Bonito lugar en la Sierra Norte de Sevilla.

.Ribera del río Huéznar a su paso por La Fundición. https://commons.wikimedia.org/wiki/File:Ribera_del_Hueznar_01.jpg[Créditos].
image::images/576px-Ribera_del_Hueznar_01.jpg[Ribera del Huéznar]

=== Isla Magica 
Isla Mágica es un parque temático situado en Sevilla, ambientado en el descubrimiento de América e inaugurado en 1997.
Se formó aprovechando algunas instalaciones de la Exposición Universal de 1992.
El parque cuenta con seis zonas temáticas y con una zona acuática llamada Agua Mágica.​
image::images/isla-magica-deudas-k4xH--1200x630@abc.jpg[Isla Magica]
